<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all();
        $options = ['Monthly','Yearly'];
        return [
            'payment_plan' => $options[array_rand($options)],
            'user_id' => $this->faker->unique()->numberBetween(1, $users->count())
        ];
    }
}
