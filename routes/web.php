<?php
use Illuminate\Support\Facades\Route;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\User\WelcomeController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\Auth\UserAuthController;
use App\Http\Controllers\User\Auth\RegisterController;
use App\Http\Controllers\User\Auth\SocialAuthController;
use App\Http\Controllers\User\MoyasarPaymentController;

Route::group(['prefix' => RouteServiceProvider::USER], function () {
    \Illuminate\Support\Facades\Config::set('auth.defines', 'web');
    Route::get('/', [WelcomeController::class, 'welcome'])->name('welcome');
    Route::get('/register', [RegisterController::class, 'getRegister'])->name('user.register');
    Route::get('/complete-register', [RegisterController::class, 'completeRegister'])->name('user.completeRegister');
    Route::post('authenticateUser', [UserAuthController::class, 'authenticateUser'])->name('user.login');
    Route::post('registerUser', [RegisterController::class, 'registerUser'])->name('registerUser');
    Route::post('choosePayment', [RegisterController::class, 'choosePayment'])->name('choosePayment');
    Route::get('pay', [MoyasarPaymentController::class, 'getPaymentForm'])->name('getPaymentForm');
    Route::get('create-payment', [MoyasarPaymentController::class, 'createPayment'])->name('createPayment');

    Route::get('auth/{driver}', [SocialAuthController::class,'redirectToProvider'])->name('social.oauth');
    Route::get('auth/{driver}/callback', [SocialAuthController::class,'handleProviderCallback'])->name('social.callback');

    Route::group(['middleware' => 'auth'], function () {
        Route::any('logout', [UserAuthController::class, 'logout'])->name('user.logout');
        Route::get('/home', [HomeController::class, 'home'])->name('user.home');
    });
});


