<?php

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\MoyasarPaymentController;

Route::group(['prefix'=>RouteServiceProvider::ADMIN], function (){
    \Illuminate\Support\Facades\Config::set('auth.defines', 'admin');
    Route::get('login',[AdminAuthController::class, 'getLogin']);
    Route::post('authenticateAdmin',[AdminAuthController::class, 'authenticateAdmin'])->name('admin.login');
    Route::group(['middleware'=>'admin:admin'], function (){
        Route::any('logout',[AdminAuthController::class, 'logout'])->name('admin.logout');
        Route::get('/',[DashboardController::class, 'dashboard'])->name('admin.dashboard');
    });

    Route::group(['prefix'=>'customers'], function (){
        Route::get('index',[UsersController::class, 'index'])->name('admin.customers.index');
        Route::get('edit/{id}',[UsersController::class, 'edit'])->name('admin.customers.edit');
        Route::post('update',[UsersController::class, 'update'])->name('admin.customers.update');
        Route::get('delete/{id}',[UsersController::class, 'delete'])->name('admin.customers.delete');
        Route::get('change-status/{id}',[UsersController::class, 'changeStatus'])->name('admin.customers.changeStatus');
    });


    Route::group(['prefix'=>'payments/moyasar'], function (){
        Route::get('index',[MoyasarPaymentController::class, 'index'])->name('admin.moyasar_payments.index');
    });
});
