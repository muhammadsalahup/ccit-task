
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Sign Up |CCIT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('images/ccit-logo.svg')}}">
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/toastr.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/sweetalert2.min.css') }}" />
</head>
<body>
<!-- Hero Start -->
<section class="bg-home bg-circle-gradiant d-flex align-items-center">
    <div class="bg-overlay bg-overlay-white"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-signin p-4 bg-white rounded shadow">
                    <form method="POST" action="{{ route('registerUser') }}">
                        @csrf
                        <a href="{{ route('welcome') }}">
                            <img
                                src="{{ asset('images/ccit-logo.svg') }}"
                                class="avatar avatar-large mb-2 d-block mx-auto"
                                style="border-radius: 20%; height: 100px"
                                alt="">
                        </a>
                        <h5 class="mb-3 text-center">Create an account</h5>

                        <div class="form-floating mb-2">
                            <input name="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   id="floatingInputw"
                                   placeholder="your full name"
                                   value="{{ old('name') }}">
                            <label for="floatingInputw">Name</label>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-floating mb-2">
                            <input name="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   id="floatingInput"
                                   placeholder="name@example.com"
                                   value="{{ old('email') }}">
                            <label for="floatingInput">Email address</label>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-3">
                            <input
                                id="floatingPassword"
                                type="password"
                                class="form-control @error('password') is-invalid @enderror"
                                name="password"
                                required
                                autocomplete="current-password">
                            <label for="floatingPassword">Password</label>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-floating mb-3">
                            <input
                                id="floatingPassword"
                                type="password"
                                class="form-control @error('password_confirmation') is-invalid @enderror"
                                name="password_confirmation"
                                required
                                autocomplete="current-password">
                            <label for="floatingPassword">Confirm Password</label>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button class="btn btn-info w-100 mb-2" type="submit">Sign up</button>
                        <a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-primary w-100 mb-2" >Facebook</a>
                        <a href="{{ route('social.oauth', 'google') }}" class="btn btn-danger w-100 mb-2" >Google</a>
                        <div class="form-floating text-muted mb-3">
                            <p>Already have an account? <a href="{{ route('welcome') }}">Login</a></p>
                        </div>

                        <p class="mb-0 text-muted mt-3 text-center">© <script>document.write(new Date().getFullYear())</script> {{ env('APP_NAME') }}.</p>
                    </form>
                </div>
            </div>
        </div>
    </div> <!--end container-->
</section><!--end section-->
<script src="{{ asset('assets/js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/js/feather.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins.init.js')}}"></script>
<script src="{{ asset('assets/js/app.js')}}"></script>
<script src="{{ asset('assets/js/plugins/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert2.min.js') }}"></script>
@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}", { timeOut: 9500 });
    </script>
@endif

@if(Session::has('error'))
    <script>
        toastr.error("{!! Session::get('error') !!}", { timeOut: 9500 });
    </script>
@endif
</body>

</html>
