
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Complete registration |CCIT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('images/ccit-logo.svg')}}">
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/toastr.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/sweetalert2.min.css') }}" />
</head>
<body>
<!-- Hero Start -->
<section class="bg-home bg-circle-gradiant d-flex align-items-center">
    <div class="bg-overlay bg-overlay-white"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-signin p-4 bg-white rounded shadow">
                    <form method="POST" action="{{ route('choosePayment') }}">
                        @csrf
                        <h5 class="mb-3 text-center">Choose Payment Plan</h5>

                        <div class="row">
                            <div class="form-floating mb-5 col-6">
                                <input name="payment_plan"
                                       type="radio"
                                       class=" @error('payment_plan') is-invalid @enderror"
                                       id="floatingInputMonthly"
                                       value="Monthly" required>
                                <label for="floatingInputMonthly">Monthly</label>
                                @error('payment_plan')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>


                            <div class="form-floating mb-5 col-6">
                                <input name="payment_plan"
                                       type="radio"
                                       class=" @error('payment_plan') is-invalid @enderror"
                                       id="floatingInputYearly"
                                       value="Yearly" required>
                                <label for="floatingInputYearly">Yearly</label>
                                @error('payment_plan')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        <button class="btn btn-primary w-100 mb-2 mt-60" type="submit">Choose</button>

                        <p class="mb-0 text-muted mt-3 text-center">© <script>document.write(new Date().getFullYear())</script> {{ env('APP_NAME') }}.</p>
                    </form>
                </div>
            </div>
        </div>
    </div> <!--end container-->
</section><!--end section-->
<script src="{{ asset('assets/js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/js/feather.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins.init.js')}}"></script>
<script src="{{ asset('assets/js/app.js')}}"></script>
<script src="{{ asset('assets/js/plugins/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert2.min.js') }}"></script>
@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}", { timeOut: 9500 });
    </script>
@endif

@if(Session::has('error'))
    <script>
        toastr.error("{!! Session::get('error') !!}", { timeOut: 9500 });
    </script>
@endif
</body>

</html>
