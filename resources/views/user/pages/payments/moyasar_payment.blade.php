<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Pay |CCIT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('images/ccit-logo.svg')}}">
    <link href="{{ asset('assets/moyasar/css/moyasar.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/moyasar/js/moyasar.js')}}"></script>
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/toastr.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/sweetalert2.min.css') }}" />
</head>
<body>
<!-- Hero Start -->
<section class="bg-home bg-circle-gradiant d-flex align-items-center">
    <div class="bg-overlay bg-overlay-white"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h5 class="mb-3 text-center">Payment Details</h5>
                <div class="mysr-form"></div>
            </div>
        </div>
    </div> <!--end container-->
</section><!--end section-->
<script src="{{ asset('assets/js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/js/feather.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins.init.js')}}"></script>
<script src="{{ asset('assets/js/app.js')}}"></script>
<script src="{{ asset('assets/js/plugins/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/sweetalert2.min.js') }}"></script>

<script>
    Moyasar.init({
        element: '.mysr-form',
        amount: 1000,
        currency: 'SAR',
        description: 'Coffee Order #1',
        publishable_api_key: 'pk_test_pUe2HQPNkqBcQKNLAvbkWzwubk3k1jtAnQULqR3a',
        callback_url: '{{ route('createPayment') }}',
        methods: [
            'creditcard',
        ],
    });
</script>
@if(Session::has('success'))
    <script>
        toastr.success("{!! Session::get('success') !!}", { timeOut: 9500 });
    </script>
@endif

@if(Session::has('error'))
    <script>
        toastr.error("{!! Session::get('error') !!}", { timeOut: 9500 });
    </script>
@endif
</body>

</html>
