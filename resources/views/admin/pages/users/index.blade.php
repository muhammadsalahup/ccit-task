@extends('admin.layouts.master')
@section('title')
    Customers |List
@endsection
@section('content')
    <div class="row row-cols-xl-12 row-cols-md-12 row-cols-1">
        <div class="col-md-4 mt-4">
            <a href="?status=All" class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                <div class="d-flex align-items-center">
                    <div class="flex-1 ms-3">
                        <h6 class="mb-0 text-muted">All Customers</h6>
                        <p class="fs-5 text-dark fw-bold mb-0"><span class="counter-value" data-target="{{ $stat['All'] }}">0</span> <span style="font-size: 10px">customers</span></p>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 mt-4">
            <a href="?status=1" class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                <div class="d-flex align-items-center">
                    <div class="flex-1 ms-3">
                        <h6 class="mb-0 text-muted">Active Customers</h6>
                        <p class="fs-5 text-dark fw-bold mb-0"><span class="counter-value" data-target="{{ $stat['Active'] }}">0</span> <span style="font-size: 10px">customers</span></p>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-4 mt-4">
            <a href="?status=0" class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                <div class="d-flex align-items-center">
                    <div class="flex-1 ms-3">
                        <h6 class="mb-0 text-muted">Inactive customers</h6>
                        <p class="fs-5 text-dark fw-bold mb-0"><span class="counter-value" data-target="{{ $stat['Inactive'] }}">0</span> <span style="font-size: 10px">customers</span></p>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-9 mt-4">
            <div class="d-flex justify-content-between p-4 bg-white shadow rounded-top">
                <h6 class="fw-bold mb-0">Customers List</h6>

                <ul class="list-unstyled mb-0">
                    <li class="dropdown dropdown-primary list-inline-item">
                        @can('customer-create')
                            <a href="{{ route('admin.customers.create') }}" class="btn btn-soft-primary"><i class="ti ti-plus"></i>Add new</a>
                        @endcan
                    </li>
                </ul>
            </div>
            <div class="table-responsive shadow rounded-bottom" data-simplebar >
                <table class="table table-center bg-white mb-0">
                    <thead>
                    <tr>
                        <th class="border-bottom p-3" style="min-width: 100px;">Customer Name</th>
                        <th class="border-bottom p-3" style="min-width: 50px;">Email</th>
                        <th class="border-bottom p-3" style="min-width: 90px;">Payment Plan</th>
                        <th class="border-bottom p-3" style="min-width: 90px;">Status</th>
                        <th class="border-bottom p-3" style="min-width: 90px;">Registration date</th>
                        <th class="text-end border-bottom p-3" style="min-width: 100px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                        <tr>
                            <td class="p-3">
                                <a href="{{ route('admin.customers.edit',$customer->id) }}" class="text-primary">
                                    <div class="d-flex align-items-center">
                                        <span class="ms-2">{{ $customer->name }}</span>
                                    </div>
                                </a>
                            </td>
                            <td class="p-1">{{ $customer->email }}</td>
                            @if(isset($customer->payment->payment_plan))
                            <td class="p-3">{{ $customer->payment->payment_plan }}</td>
                            @else
                                <td class="p-3">unsubscribe</td>
                            @endif

                            @if($customer->status == "Active")
                                <td>
                                    <a href="{{ route('admin.customers.changeStatus',$customer->id) }}" class="badge bg-soft-success rounded px-3 py-1">
                                        Active
                                    </a>
                                </td>
                            @else
                                <td>
                                    <a href="{{ route('admin.customers.changeStatus',$customer->id) }}" class="badge bg-soft-danger rounded px-3 py-1">
                                        Inactive
                                    </a>
                                </td>
                            @endif
                            <td class="p-3">{{ $customer->created_at->diffForHumans() }}</td>
                            <td class="text-end p-3">
                                <a title="edit customer" href="{{ route('admin.customers.edit',$customer->id) }}" class="btn btn-sm btn-primary"><i class="ti ti-edit"></i></a>
                                <a title="delete customer" class="btn btn-sm btn-soft-danger ms-2 delete-confirm" href="{{ route('admin.customers.delete',$customer->id) }}" ><i class="ti ti-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $customers->links('pagination::bootstrap-4')}}
                </div>
            </div>
        </div><!--end col-->
        <div class="col-xl-3 mt-4">
            <form method="get" action="{{ route('admin.customers.index') }}">
                @csrf
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card rounded shadow p-4 border-0">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 mt-4">
                                    <div class="row g-3">
                                        <div class="col-sm-12">
                                            <label for="name" class="form-label">Customer name</label>
                                            <input
                                                type="text"
                                                name="name"
                                                class="form-control @error('name') is-invalid @enderror"
                                                id="name"
                                                placeholder="Customer name "
                                                value="{{ \Session::get('name') }}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                        <div class="col-sm-12">
                                            <label for="email" class="form-label">Customer email</label>
                                            <input
                                                type="text"
                                                name="email"
                                                class="form-control @error('email') is-invalid @enderror"
                                                id="name"
                                                placeholder="Customer email"
                                                value="{{ \Session::get('email') }}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="form-label">Status</label>
                                            <select name="status"
                                                    class="form-control @error('status') is-invalid @enderror">
                                                <option value="All" @if(\Session::get('status') == 'All') selected @endif>All</option>
                                                <option value="Active" @if(\Session::get('status') == "Active") selected @endif>Active ({{ $stat['Active'] }})</option>
                                                <option value="Inactive" @if(\Session::get('status') == "Inactive") selected @endif>Inactive ({{ $stat['Inactive'] }})</option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div><!--end col-->
                            </div>
                            <div class="row g-3">
                                <button class="w-100 btn btn-primary mt-4" type="submit">Filter</button>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </form>
        </div>
    </div><!--end row-->
    <script>
        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: 'Are you sure?',
                text: 'This record and it`s details will be permanently deleted!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'

            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        });
    </script>
@endsection
