@extends('admin.layouts.master')
@section('title')
    Customer|Edit
@endsection

@section('content')
    <div class="container-fluid">
        <div>
            <div class="d-md-flex justify-content-between align-items-center">
                <h5 class="mb-2">Edit Customer</h5>
            </div>
            <form method="POST" action="{{ route('admin.customers.update') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <input type="hidden" name="id" value="{{ $customer->id }}">
                    <div class="col-md-12 col-lg-12">
                        <div class="card rounded shadow p-4 border-0">
                            <h4 class="mb-3">Basic Info</h4>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 mt-4">
                                    <div class="row g-3">
                                        <div class="col-sm-12">
                                            <label for="name" class="form-label">Full Name</label>
                                            <input
                                                type="text"
                                                name="name"
                                                class="form-control @error('name') is-invalid @enderror"
                                                id="name"
                                                placeholder="type customer name"
                                                value="{{ $customer->name }}"
                                                required>
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-12">
                                            <label for="email" class="form-label">Email</label>
                                            <input
                                                type="email"
                                                name="email"
                                                class="form-control @error('email') is-invalid @enderror"
                                                id="email"
                                                placeholder="Email Address"
                                                value="{{ $customer->email }}"
                                                disabled
                                                required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <label for="email" class="form-label">Payment Plan</label>

                                        <div class="col-sm-4">
                                            <input
                                                type="radio"
                                                name="payment_plan"
                                                class=" @error('payment_plan') is-invalid @enderror"
                                                id="payment_plan"
                                                value="Monthly"
                                                @if(isset($customer->payment->payment_plan))
                                                    @if($customer->payment->payment_plan == "Monthly")
                                                        checked
                                                    @endif
                                                @endif
                                                required>
                                            <label for="payment_plan" class="form-label">Monthly</label>

                                            @error('payment_plan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-4">
                                            <input
                                                type="radio"
                                                name="payment_plan"
                                                class="@error('payment_plan') is-invalid @enderror"
                                                id="payment_plan"
                                                @if(isset($customer->payment->payment_plan))
                                                    @if($customer->payment->payment_plan == "Yearly")
                                                        checked
                                                    @endif
                                                @endif
                                                value="Yearly"
                                                required>
                                            <label for="payment_plan" class="form-label">Yearly</label>

                                            @error('payment_plan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-4">
                                            <input
                                                type="radio"
                                                name="payment_plan"
                                                class=" @error('payment_plan') is-invalid @enderror"
                                                id="payment_plan"
                                                value="unsubscribe"
                                                @if(!in_array($customer->payment->payment_plan,['Monthly','Yearly']))
                                                    checked
                                                @endif
                                                required>
                                            <label for="payment_plan" class="form-label">Unsubscribe</label>

                                            @error('payment_plan')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div><!--end col-->
                            </div>
                            <div class="row g-3">
                                <button class="w-100 btn btn-primary mt-4" type="submit">Change</button>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </form>
        </div>
    </div><!--end container-->
@endsection
