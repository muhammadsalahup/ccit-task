@extends('admin.layouts.master')
@section('title')
    {{ env('APP_NAME') }} | Dashboard
@endsection
@section('content')
    <div class="alert alert-success col-12" role="alert">
        <h6 class="text-white mb-1">Welcome back, {{ auth()->guard('admin')->user()->name }}!</h6>
    </div>
    <div class="d-flex align-items-center justify-content-between">
        <div>
            <h5 class="mb-0">Dashboard</h5>
        </div>
    </div>

    <div class="row row-cols-xl-12 row-cols-md-12 row-cols-1">
        <div class="col-md-12 mt-4">
            <a href="#" class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                <div class="d-flex align-items-center">
                    <div class="icon text-center rounded-pill">
                        <i class="ti ti-users fs-4 mb-0"></i>
                    </div>
                    <div class="flex-1 ms-3">
                        <h6 class="mb-0 text-muted">Customers</h6>
                        <p class="fs-5 text-dark fw-bold mb-0"><span class="counter-value" data-target="{{ $stat['All'] }}">0</span></p>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
