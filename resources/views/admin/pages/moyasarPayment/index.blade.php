@extends('admin.layouts.master')
@section('title')
    Customers |List
@endsection
@section('content')
    <div class="row row-cols-xl-12 row-cols-md-12 row-cols-1">
        <div class="col-md-12 mt-4">
            <a href="?status=All" class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                <div class="d-flex align-items-center">
                    <div class="flex-1 ms-3">
                        <h6 class="mb-0 text-muted">All Payments</h6>
                        <p class="fs-5 text-dark fw-bold mb-0"><span class="counter-value" data-target="{{ $stat['All'] }}">0</span> <span style="font-size: 10px">customers</span></p>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-9 mt-4">
            <div class="d-flex justify-content-between p-4 bg-white shadow rounded-top">
                <h6 class="fw-bold mb-0">Customers List</h6>

                <ul class="list-unstyled mb-0">
                    <li class="dropdown dropdown-primary list-inline-item">
                        @can('customer-create')
                            <a href="{{ route('admin.customers.create') }}" class="btn btn-soft-primary"><i class="ti ti-plus"></i>Add new</a>
                        @endcan
                    </li>
                </ul>
            </div>
            <div class="table-responsive shadow rounded-bottom" data-simplebar >
                <table class="table table-center bg-white mb-0">
                    <thead>
                    <tr>
                        <th class="border-bottom p-3" style="min-width: 100px;">Payment ID</th>
                        <th class="border-bottom p-3" style="min-width: 100px;">Customer Name</th>
                        <th class="border-bottom p-3" style="min-width: 50px;">Email</th>
                        <th class="border-bottom p-3" style="min-width: 90px;">Payment Plan</th>
                        <th class="border-bottom p-3" style="min-width: 90px;">Payment date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td class="p-1">{{ $payment->payment_id }}</td>
                            <td class="p-3">
                                <a href="{{ route('admin.customers.edit',$payment->user->id) }}" class="text-primary">
                                    <div class="d-flex align-items-center">
                                        <span class="ms-2">{{ $payment->user->name }}</span>
                                    </div>
                                </a>
                            </td>
                            <td class="p-1">{{ $payment->user->email }}</td>
                            @if(isset($payment->user->payment->payment_plan))
                                <td class="p-3">{{ $payment->user->payment->payment_plan }}</td>
                            @else
                                <td class="p-3">unsubscribe</td>
                            @endif

                            <td class="p-3">{{ $payment->created_at->diffForHumans() }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div class="mt-4">
                    {{ $payments->links('pagination::bootstrap-4')}}
                </div>
            </div>
        </div><!--end col-->
        <div class="col-xl-3 mt-4">
            <form method="get" action="{{ route('admin.moyasar_payments.index') }}">
                @csrf
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card rounded shadow p-4 border-0">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 mt-4">
                                    <div class="row g-3">
                                        <div class="col-sm-12">
                                            <label for="name" class="form-label">Payment ID</label>
                                            <input
                                                type="text"
                                                name="payment_id"
                                                class="form-control @error('payment_id') is-invalid @enderror"
                                                id="name"
                                                placeholder="Customer name "
                                                value="{{ \Session::get('payment_id') }}">
                                            @error('payment_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="name" class="form-label">Customer name</label>
                                            <input
                                                type="text"
                                                name="name"
                                                class="form-control @error('name') is-invalid @enderror"
                                                id="name"
                                                placeholder="Customer name "
                                                value="{{ \Session::get('user_name_payment') }}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                        <div class="col-sm-12">
                                            <label for="email" class="form-label">Customer email</label>
                                            <input
                                                type="text"
                                                name="email"
                                                class="form-control @error('email') is-invalid @enderror"
                                                id="name"
                                                placeholder="Customer email"
                                                value="{{ \Session::get('email_payment') }}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div><!--end col-->
                            </div>
                            <div class="row g-3">
                                <button class="w-100 btn btn-primary mt-4" type="submit">Filter</button>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </form>
        </div>
    </div><!--end row-->
@endsection
