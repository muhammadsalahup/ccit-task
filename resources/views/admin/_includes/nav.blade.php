<nav id="sidebar" class="sidebar-wrapper sidebar-dark">
    <div class="sidebar-content" data-simplebar style="height: calc(100% - 60px);">
        <div class="sidebar-brand">
            <a href="{{ route('welcome') }}" target="_blank">
                <img src="{{ asset('images/ccit-logo.svg') }}" height="24" class="logo-light-mode" alt="">
                <img src="{{ asset('images/ccit-logo.svg') }}" height="24" class="logo-dark-mode" alt="">
                <span class="sidebar-colored">
                    <img src="{{ asset('images/ccit-logo.svg') }}" height="24" alt="">
                </span>
            </a>
        </div>
        <ul class="sidebar-menu">
            <li><a href="{{ route('admin.dashboard') }}"><i class="ti ti-home me-2"></i>Dashboard</a></li>
            <li><a href="{{ route('admin.customers.index') }}"><i class="ti ti-users me-2"></i>Customers</a></li>
            <li><a href="{{ route('admin.moyasar_payments.index') }}"><i class="ti ti-currency-dollar me-2"></i>Moyasar Payments</a></li>
        </ul>
        <!-- sidebar-menu  -->
    </div>
</nav>
