<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('images/ccit-logo.svg')}}">
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/simplebar.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/tabler-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.min.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/toastr.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/sweetalert2.min.css') }}" />
    <style>
        .thumb{
            margin: 10px 5px 0 0;
            width: 220px;
        }
    </style>
</head>
