<footer class="bg-white shadow py-3">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col">
                <div class="text-sm-start text-center">
                    <p class="mb-0 text-muted">
                        © <script>document.write(new Date().getFullYear())</script>
                        {{ env('APP_NAME') }} Admin Panel. Design with
                        <i class="ti ti-heart text-danger"></i>
                        by <a href="mailto:muhammadsalahup@gmail.com" target="_blank" class="text-reset">muhammadsalahup@gmail.com</a>.
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer>
