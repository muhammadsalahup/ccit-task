<!DOCTYPE html>
<html lang="en">
    @include('admin._includes.head')
    <body>
        <div class="page-wrapper landrick-theme toggled">
            @include('admin._includes.nav')
            <main class="page-content bg-light">
                @include('admin._includes.header')
                <div class="container-fluid">
                    <div class="layout-specing">
                        @yield('content')
                    </div>
                </div>
                @include('admin._includes.footer')
            </main>
        </div>
        @include('admin._includes.scripts')
        @yield('plugins')
    </body>
</html>
