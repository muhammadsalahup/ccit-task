<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MoyasarPaymentController extends Controller
{
    public function getPaymentForm()
    {
        return view('user.pages.payments.moyasar_payment');
    }
    public function createPayment(Request $request)
    {
        $payment = auth()
            ->guard('web')
            ->user()
            ->moyasarPayments()
            ->create($this->formatRequest($request->all()));
        return redirect()
            ->route('user.home')
            ->with('success','Thanks for payment '. auth()->guard('web')->user()->name);
    }

    protected function formatRequest(array $request)
    {
        $request['payment_id'] = $request['id'];
        return $request;
    }
}
