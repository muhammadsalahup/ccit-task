<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\RegisterRequest;
use App\Services\User\RegisterService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    public function __construct(RegisterService $registerService)
    {
        $this->middleware('guest');
        $this->registerService = $registerService;
    }

    public function getRegister()
    {
        return view('user.pages.auth.register');
    }

    public function completeRegister()
    {
        return view('user.pages.auth.complete_register');
    }

    public function registerUser(RegisterRequest $request)
    {
        $user = $this->registerService->createUser($request->validated());
        Session::put('user_id',$user->id);
        return redirect()->route('user.completeRegister')
            ->with('success','Complete Registration ,please');
    }

    public function choosePayment(Request $request)
    {
        $user = $this->registerService->createPaymentPlan($request->all());
        $this->guard()->login($user);
        return redirect()->route('getPaymentForm')
            ->with('success','Thanks you '. auth()->guard('web')->user()->name. ' ,please complete final step in order to have a paid account');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }
}
