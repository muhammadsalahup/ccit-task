<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Helper\AuthRequest;
use App\Services\Helpers\AuthService;
use App\Services\User\GetUserService;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->authService = new AuthService('web');
    }

    public function authenticateUser(AuthRequest $request ,GetUserService $getUserService)
    {
        $user = $getUserService->getByEmail($request->email);
        if ($user && $user->status == "Inactive")
        {
            return redirect()->route('welcome')
                ->with(['error' =>  'Unable to login , Inactive account']);
        }

        if($this->authService->authenticate($request->only('email', 'password')))
        {
            return redirect()->route('user.home')->with('success','Successfully Logged In');
        }else{
            return redirect()->back()->with('error','Invalid Credentials');
        }
    }

    public function logout()
    {
        $this->authService->logout();
        return redirect('/');
    }
}
