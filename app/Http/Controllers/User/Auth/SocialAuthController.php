<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Services\Helpers\SocialAuthService;
use Laravel\Socialite\Facades\Socialite;
use Exception;

class SocialAuthController extends Controller
{

    public function __construct(SocialAuthService $socialAuthService)
    {
        $this->middleware('guest');
        $this->socialAuthService = $socialAuthService;
    }

    protected $providers = [
        'facebook','google'
    ];

    public function redirectToProvider($driver)
    {
        if( ! $this->isProviderAllowed($driver) ) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        try {
            return Socialite::driver($driver)->redirect();
        } catch (Exception $e) {
            // You should show something simple fail message
            return $this->sendFailedResponse($e->getMessage());
        }
    }

    public function handleProviderCallback( $driver )
    {
        try {
            $user = Socialite::driver($driver)->user();
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        return empty( $user->email )
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
    }

    protected function sendSuccessResponse()
    {
        return redirect()->to('/home');
    }

    protected function sendFailedResponse($msg = null)
    {
        return redirect()->route('welcome')
            ->with(['error' => $msg ?: 'Unable to login, try with another provider to login.']);
    }

    protected function loginOrCreateAccount($providerUser, $driver)
    {
        $user = $this->socialAuthService->loginOrCreate($providerUser, $driver);
        if ($user->status == "Inactive")
            return redirect()->route('welcome')
                ->with(['error' =>  'Unable to login , Inactive account']);
        auth()->guard('web')->login($user);
        return $this->sendSuccessResponse();
    }

    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }

}
