<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\User\ChangeStatusService;
use App\Services\User\DeleteUserService;
use App\Services\User\GetUserService;
use App\Services\User\UpdateUserService;
use Illuminate\Http\Request;
use Exception;
class UsersController extends Controller
{
    public function index(Request $request ,GetUserService $getUserService)
    {
        $customers = $getUserService
            ->getAll($request->all())
            ->orderBy("id","DESC")
            ->paginate(8);
        $stat = $getUserService->getUserStat();
        return view('admin.pages.users.index')
            ->with(compact('customers','stat'));
    }


    public function edit(
        int $id,
        GetUserService $getUserService
    )
    {
        $customer = $getUserService->getById($id);
        return view('admin.pages.users.edit')
            ->with(compact('customer'));
    }

    public function update(Request $request ,UpdateUserService $updateUserService)
    {
        $updateUserService->update($request->all());
        return redirect()->back()->with('success','Customer Updated successfully');
    }

    public function changeStatus(int $id ,ChangeStatusService $changeStatusService)
    {
        $changeStatusService->change($id);
        return redirect()->back()->with('success','Successfully changed');
    }

    public function delete(int $id ,DeleteUserService $deleteUserService)
    {
        $deleteUserService->delete($id);
        return redirect()->back()->with('success','Customer deleted successfully');
    }

}
