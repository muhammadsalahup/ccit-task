<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Payment\Moyasar\MoyasarGetPaymentService;
use Illuminate\Http\Request;

class MoyasarPaymentController extends Controller
{
    public function index(Request $request ,MoyasarGetPaymentService $moyasarGetPaymentService)
    {
        $payments = $moyasarGetPaymentService
            ->getAll($request->all())
            ->orderBy("id","DESC")
            ->paginate(6);
        $stat = $moyasarGetPaymentService->getPaymentStat();
        return view('admin.pages.moyasarPayment.index')
            ->with(compact('payments','stat'));
    }
}
