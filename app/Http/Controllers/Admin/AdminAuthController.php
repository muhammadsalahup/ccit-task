<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Helper\AuthRequest;
use App\Providers\RouteServiceProvider;
use App\Services\Helpers\AuthService;
use Illuminate\Http\Request;

class AdminAuthController extends Controller
{
    public function __construct()
    {
        $this->authService = new AuthService('admin');
    }
    public function getLogin()
    {
        return view('admin.pages.login');
    }

    public function authenticateAdmin(AuthRequest $request)
    {
        if($this->authService->authenticate($request->only('email', 'password')))
        {
            return redirect(RouteServiceProvider::ADMIN);
        }else{
            session()->flash('error', 'Invalid Credentials');
            return redirect()->back()->with('error', 'Invalid Credentials');
        }
    }

    public function logout()
    {
        $this->authService->logout();
        return redirect(RouteServiceProvider::ADMINLOGIN);
    }
}
