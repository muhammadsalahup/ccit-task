<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\User\GetUserService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard(GetUserService $getUserService)
    {
        $stat = $getUserService->getUserStat();
        return view('admin.pages.dashboard',compact('stat'));
    }
}
