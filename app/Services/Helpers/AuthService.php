<?php

namespace App\Services\Helpers;

class AuthService
{
    public $guard;
    public function __construct(string $guard)
    {
        $this->guard = $guard;
    }
    public function authenticate(array $credentials) : bool
    {
        return auth()->guard($this->guard )->attempt($credentials);
    }

    public function logout()
    {
        return auth($this->guard)->logout();
    }
}
