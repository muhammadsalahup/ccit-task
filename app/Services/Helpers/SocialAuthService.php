<?php

namespace App\Services\Helpers;

use App\Models\User;
use App\Services\User\GetUserService;
use App\Services\User\RegisterService;
use Illuminate\Support\Facades\Hash;

class SocialAuthService
{
    public function __construct(RegisterService $registerService, GetUserService $getUserService)
    {
        $this->registerService = $registerService;
        $this->getUserService = $getUserService;
    }

    public function loginOrCreate($providerUser, $driver)
    {
        $user = $this->getUserService->getByEmail($providerUser->getEmail());

        if($user) {
            $user->update([
                'avatar' => $providerUser->avatar,
                'provider' => $driver,
                'provider_id' => $providerUser->id,
                'access_token' => $providerUser->token
            ]);
        } else {
            $user = $this->registerService->registerSocialUser([
                'name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'avatar' => $providerUser->getAvatar(),
                'provider' => $driver,
                'provider_id' => $providerUser->getId(),
                'access_token' => $providerUser->token,
                'password' => Hash::make($providerUser->token)
            ]);
        }
        return $user;
    }
}
