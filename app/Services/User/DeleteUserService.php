<?php

namespace App\Services\User;

use App\Models\User;
use Exception;

class DeleteUserService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function delete(int $id)
    {
        try {
            $user = $this->user->findOrFail($id);
            $user->delete();
        } catch (Exception $exception) {
            throw new Exception($exception);
        }
        return $user;
    }
}
