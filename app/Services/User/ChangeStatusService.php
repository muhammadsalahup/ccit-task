<?php

namespace App\Services\User;

use App\Models\User;

class ChangeStatusService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function change(int $id)
    {
        $user = $this->user->findOrFail($id);
        ($user->status == 'Active') ? $user->update(['status'=>"Inactive"]) : $user->update(['status'=>"Active"]);
        return $user;
    }
}
