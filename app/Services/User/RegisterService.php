<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class RegisterService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        $user = $this->user->create($data);
        return $user;
    }

    public function registerSocialUser(array $data)
    {
        $data['password'] = Hash::make($data['access_token']);
        $user = $this->user->create($data);
        return $user;
    }


    public function createPaymentPlan(array $data)
    {
        $user = $this->user->find(Session::get('user_id'));
        $user->payment()->create($data);
        return $user;
    }
}
