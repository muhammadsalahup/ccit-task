<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Exception;
class UpdateUserService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function update(array $data)
    {
        DB::beginTransaction();
        try {
            $user = $this->user->findOrFail($data['id']);
            $user->update($data);
            $user->payment()->update(['payment_plan'=>$data['payment_plan']]);
        } catch (Exception $exception) {
            DB::rollback();
            throw new Exception($exception);
        }
        DB::commit();
        return $user;
    }
}
