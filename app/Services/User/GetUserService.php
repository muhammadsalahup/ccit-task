<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Session;


class GetUserService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getAll(array $filters)
    {
        if ($filters != null)
            $this->cacheInputs($filters);
        $query = $this->user->newQuery();

        if (isset($filters['name']))
            $query->where('name','like', '%' . $filters['name'] . '%');

        if (isset($filters['email']))
            $query->where('email','like', '%' . $filters['email'] . '%');

        if (isset($filters['status']) && $filters['status'] != "All")
            $query->where('status',$filters['status']);

        return $query;
    }

    public function getById(int $id)
    {
        return $this->user->findOrFail($id);
    }

    public function getByEmail(string $email)
    {
        return $this->user->where('email',$email)->first();
    }

    public function getUserStat()
    {
        $query = $this->user;
        return [
            'All' => $query->count(),
            'Active' => $query->where('status',"Active")->count(),
            'Inactive' => $query->where('status',"Inactive")->count(),
        ];
    }

    private function cacheInputs(array $filters)
    {
        Session::put([
            'name' => (isset($filters['name'])) ? $filters['name'] : '',
            'email' => (isset($filters['email'])) ? $filters['email'] : '',
            'status' => (isset($filters['status'])) ? $filters['status'] : '',
        ]);
    }
}
