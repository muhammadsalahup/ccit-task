<?php

namespace App\Services\Payment\Moyasar;

use App\Models\MoyasarPayment;
use Illuminate\Support\Facades\Session;

class MoyasarGetPaymentService
{
    public function __construct(MoyasarPayment $moyasarPayment)
    {
        $this->moyasarPayment = $moyasarPayment;
    }

    public function getAll(array $filters)
    {
        if ($filters != null)
            $this->cacheInputs($filters);
        $query = $this->moyasarPayment->newQuery();

        if (isset($filters['payment_id']))
            $query->where('payment_id','like', '%' . $filters['payment_id'] . '%');

        if (isset($filters['name']))
            $query->whereHas('user',function(\Illuminate\Database\Eloquent\Builder $q) use ($filters){
                return $q->where('name','like', '%' . $filters['name'] . '%');
            });

        if (isset($filters['email']))
            $query->whereHas('user',function(\Illuminate\Database\Eloquent\Builder $q) use ($filters){
                return $q->where('email','like', '%' . $filters['email'] . '%');
            });

        return $query;
    }

    public function getPaymentStat()
    {
        $query = $this->moyasarPayment;
        return [
            'All' => $query->count(),
        ];
    }

    private function cacheInputs(array $filters)
    {
        Session::put([
            'payment_id' => (isset($filters['payment_id'])) ? $filters['payment_id'] : '',
            'user_name_payment' => (isset($filters['name'])) ? $filters['name'] : '',
            'email_payment' => (isset($filters['email'])) ? $filters['email'] : '',
            'status_payment' => (isset($filters['status'])) ? $filters['status'] : '',
        ]);
    }
}
