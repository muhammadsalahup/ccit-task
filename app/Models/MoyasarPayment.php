<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoyasarPayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'payment_id', 'status', 'amount','message'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
